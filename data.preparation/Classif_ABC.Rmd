---
title: "Classification de gènes en partenaires ABC"
author: "Alexia Rivero, Antoine Guillemin"
date: "14/04/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Chargement des librairies :

```{r}
library(tidyverse)
library(dplyr)
```

Chargement des données :

```{r}
gene = read.delim("Gene.tsv", sep = "\t", header = T)
protein = read.delim("Protein.tsv", sep = "\t", header = T)
orthology = read.delim("Orthology.tsv", sep = "\t", header = T)
cdomain = read.delim("Conserved_Domain.tsv", sep = "\t", header = T)
fdomain = read.delim("Functional_Domain.tsv", sep = "\t", header = T)
```

Aperçu des données :

- La classe que l'on cherche à prédire est ABC ou non. Cette classe est déterminée par l'attribut "Type" de protein.

```{r}
summary(protein)
```

On constate qu'il y a 15416 séquences considérées comme codant pour un partenaire ABC sur un total de 15554 séquences. Jeu de données trop déséquilibré pour construire un modèle fiable ?

On peut afficher les premières lignes de la table Conserved_Domains :

```{r}
head(cdomain)

dim(cdomain)
```

On peut chercher à "nettoyer" les données afin de réduire leur volume. Par exemple ici, il peut être intéressant de ne sélectionner que les domaines conservés dont l'e-value est inférieure à un seuil défini arbitrairement (exemple : <= 1e-05).

```{r}
cd_filtered = cdomain %>% filter(e_Value <= 1e-05)

head(cd_filtered)

dim(cd_filtered)
```


- On extrait les colonnes "Gene_ID" et "Type" de protein, et "FD_ID" et "FD_Name" de Functional_Domain :

```{r}
gene_id_type = protein %>% select(Gene_ID, Type)
fd_name = fdomain %>% select(FD_ID, FD_Name)
```

- On ajoute le nom du domaine fonctionnel correspondant au domaine conservés au tableau des CDs filtrés. On ajoute ensuite la classe (ABC ou non) au tableau ainsi obtenu en réalisant une jointure entre ce dernier et le tableau gene_id_type, sur l'attribut Gene_ID.

```{r}
cd_name = cd_filtered %>% inner_join(fd_name, by = "FD_ID")
cd_name_type = cd_name %>% inner_join(gene_id_type, by = "Gene_ID")
head(cd_name_type)
```

- On sélectionne tout sauf la classe (donc la colonne "Type" ajoutée précédemment) dans m ;

- on sélectionne la classe dans y.

```{r}
m = cd_name_type[, -11]
head(m)
y = cd_name_type[, 11]
```

- Tableau de contingence : classe selon le nom du domaine fonctionnel.

```{r}
table( m[ , 'FD_Name'], y)
```

- Test du chi2 :

```{r}
chisq.test(m[ , 'FD_Name'], y)
```

p-value < 2.2e-16 : il y aurait un lien entre la classe et le nom du DF ?

Problèmes : impossible de représenter graphiquement le tableau de contingence (illisible), difficile également de représenter l'attribut qui pourrait classer les gènes de manière parfaite - qui donnerait une répartition très déséquilibrée (temps d'affichage trop long apparemment). 

Nécessité de "nettoyer les données" ?

```{r}
colnames(gene_id_type)[1] = "subject_id"
colnames(gene_id_type)
```

Ajout du type pour Orthology (subject) :

```{r}
subject_abc = left_join(orthology, gene_id_type, by=c("subject_id"))
head(subject_abc)
# write_tsv(subject_abc, "orthology_abc.tsv")
```

Nombre de subject_id de type ABC avec evalue significative par query_id :

```{r}
nb_subject = subject_abc %>% filter(evalue <= 1e-05 | Type == 'ABC') %>%  group_by(query_id, .drop=FALSE) %>% summarize(subject_id = n())
head(nb_subject)
```

```{r}
colnames(gene_id_type)[1] = "Gene_ID"
colnames(nb_subject)[1] = "Gene_ID" 
```

Ajout d'une colonne comptant le nombre d'orthologues ABC :

```{r}
nb_orthologs = left_join(gene_id_type, nb_subject, by=c("Gene_ID"))
colnames(nb_orthologs)[3] = "Nb_ortho_ABC"
```

Remplacement des NA par 0 :

```{r}
nb_orthologs[is.na(nb_orthologs)] = 0
```

Pour ne pas avoir ID en format scientifique : (exemple)

```{r}
protein = format(protein, scientific = F)
```

Tri des valeurs de FD_ID 
```{r}
unique(cdomain$FD_ID)
```

```{r}
library(RMySQL)
dbh=dbConnect(MySQL(),
              user="root",
              password="bioinfo", 
              dbname="fouille", 
              host="localhost")

query = "SELECT FD_ID, COUNT(Gene_ID) FROM Conserved_Domain GROUP BY FD_ID"
df = dbGetQuery(dbh, query)
knitr::kable(df[1:20,])
df

```

On se concentrera uniquement sur les FD_ID étant présent au moins dans 50 gènes = 1876

```{r}
query = "SELECT COUNT(FD_ID) FROM Conserved_Domain WHERE Gene_ID IN(SELECT Gene_ID FROM Conserved_Domain GROUP BY Gene_ID HAVING COUNT(Gene_ID) > 50)"
dbGetQuery(dbh, query)
```

Création du data frame : 
```{r}

tab = dbGetQuery(dbh, "SELECT FD_ID, COUNT(Gene_ID) FROM Conserved_Domain GROUP BY FD_ID") %>% as_tibble
tab %>% select(FD_ID)

fd = tab %>% filter(`COUNT(Gene_ID)`>50) 
fd

which(fd$`COUNT(Gene_ID)`>100)
```

