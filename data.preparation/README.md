Scripts accompagnés de la documentation utilisateur permettant d'obtenir la matrice *individus-variables* qui servira pour le projet.

# Documentation utilisateur

Ce document décrit comment obtenir une matrice permettant de réaliser une classification de gènes selon s’ils codent pour des partenaires ABC ou non.

## Prérequis

### Données

Les données utilisées sont issues des fichiers.tsv correspondant aux tables suivantes :
- Protein
- Conserved_Domain
- Orthology

Pour la réalisation de la matrice, ces fichiers doivent être disponibles dans le répertoire de travail au format .tsv et les tables correspondantes doivent être chargées dans une base de données MySQL.

### Librairies R

Plusieurs librairies R sont nécessaires pour exécuter le script script_matrice.R :
- data.table pour le chargement des données dans R ;
- tidyverse pour la manipulation des tableaux, notamment pour les opérations de jointures ;
- RMySQL pour pouvoir se connecter au serveur MySQL.

## Etapes du script

### Paramètres

Le répertoire de travail doit être celui contenant les fichiers .tsv du projet. Il peut être défini avec la commande setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) si le script est déjà placé dans ce répertoire. Sinon, il peut être modifié manuellement par l’utilisateur dans R avec Session -> Set Working Directory -> ...

Le chargement des différentes librairies est réalisé au début du script.

Enfin, le paramètre options(scipen = 999) permet de désactiver la notation scientifique, ce qui est utile pour les identifiants de gènes notamment. Pour réinitialiser ce paramètre plus tard, il suffit d’entrer options(scipen = 0).

### Chargement des données

Le chargement des données est réalisé à l’aide de la fonction fread de la librairie data.table. Cette fonction, similaire à read.table, permet un chargement plus rapide des données. Cependant, son utilisation nécessitera de convertir certaines données en format numérique pour réaliser les opérations de jointures par la suite. 


### Construction de la matrice

#### Classe : Type

Le tableau Protein contient les identifiants de gènes (Gene ID) codant pour des protéines. La classe d’intérêt est déterminée par la colonne Type qui indique pour chaque identifiant si le gène code pour un partenaire ABC ou autre. 

#### Création des attributs FD ID

Le tableau Conserved Domain fait notamment correspondre les Gene ID avec des identifiants de domaines fonctionnels présents sur ces gènes (FD ID). La présence ou l’absence de certains domaines sur les gènes pourrait permettre de classer ces derniers en gènes codant pour des partenaires ABC ou non.

Pour extraire les informations concernant les FD ID du tableau Conserved Domain, il est possible d’utiliser une requête SQL. Il faut établir dans un premier temps une connexion au serveur MySQL grâce à la fonction dbConnect de la librairie RMySQL. Les paramètres à indiquer sont principalement user, password, dbname et hostname qui dépendent de l’utilisateur. La requête SQL permet de filtrer les FD ID : l’e-value attribuée doit être inférieure ou égale à 1e-05 et le gène correspondant doit coder pour une protéine. Au final, un vecteur contenant uniquement les FD ID sélectionnés est extrait.

Création de la matrice :
- lignes (individus) = Gene ID du tableau Protein ;
- colonnes (variables) = FD ID sélectionnés + colonne Gene ID + colonne Type. Une dernière colonne sera ajoutée plus loin dans le script.
Chaque cellule de la matrice, pour les colonnes FD ID, est initialisée à “absence”.

Remplissage de la matrice :
Le vecteur contenant les FD ID sélectionnés précédemment est transformé en tableau à 1 colonne. Une jointure est réalisée entre ce nouveau tableau et le tableau Conserved Domain pour obtenir un tableau temporaire tmp contenant uniquement les lignes correspondant aux FD ID sélectionnés.
Ensuite, la boucle for permet, pour chaque Gene ID du tableau tmp, si celui-ci correspond à un Gene ID de la matrice individus x variables (donc codant pour une protéine), d’affecter "presence" à la colonne de chaque FD ID qu'il contient. Remarque : la boucle peut nécessiter un temps d’exécution plus ou moins long, mais qui n’excédait pas 2 min d’après les tests réalisés.

#### Création de l’attribut nombre d'orthologues ABC

Cet attribut correspond, pour chaque ID de gène codant pour une protéine, au nombre d’orthologues avec une e-value attribuée par BLAST inférieure ou égale à 0,5 et prédits comme codant pour des partenaires ABC.

Un tableau contenant 2 colonnes est créé par requête SQL (en utilisant la connexion précédente) à partir du tableau Orthology :
- colonne 1 : Gene ID
- colonne 2 : nombre d’orthologues avec une e-value <= 1e-05 dans Orthology et prédits comme étant ABC.

Une jointure à gauche est ensuite réalisée entre la matrice créée à l’étape 3) et ce tableau sur la colonne Gene ID, afin de créer la nouvelle colonne correspondant au nombre d'orthologues ABC dans la matrice finale. Ce type de jointure permet d’adjoindre les informations du tableau de droite au tableau de gauche : c’est donc la matrice de l’étape 3) qui définit les individus en sortie. Il est nécessaire de convertir la colonne Gene ID de cette matrice au format numérique avant de réaliser la jointure (la fonction fread utilisée pour charger les données attribue le type integer64 à la colonne, ce qui rend impossible la jointure). Enfin, certains Gene ID possèdent la valeur NA dans la colonne du nombre d'orthologues, il faut donc la remplacer par 0.

La matrice finale obtenue contient donc :
- les identifiants de gènes codant pour des protéines en lignes ;
- des colonnes correspondant aux identifiants de domaines fonctionnels, indiqués comme présents ou absents pour chaque gène ;
- le type correspondant à chaque identifiant de gène ;
- le nombre d'orthologues codant pour des partenaires ABC pour chaque gène.

## Utilisation de la matrice

La matrice créée pourra être utilisée pour déterminer un modèle permettant de distinguer les gènes codant pour des partenaires ABC (de classe ABC) des gènes d’autres classes. Ce modèle devra donc classer les individus en lignes (Gene ID), les classes étant déterminées par les différentes modalités de l’attribut Type. Cette classification sera réalisée en fonction des FD ID présents ou absents sur les gènes et/ou du nombre d’orthologues ABC (attributs en colonnes).

Par exemple, le fichier de sortie du script R pourra être chargé dans KNIME à l’aide d’un File Reader. En configurant un Decision Tree ou un Naive Bayes Learner selon les paramètres disponibles, il sera possible d’obtenir un modèle. Une évaluation des performances de chaque modèle produit pourra être réalisée (par exemple, à l’aide de validations croisées) de manière à estimer les taux d’erreur. L’intérêt sera principalement de modifier les différents paramètres à chaque étape pour observer l’influence sur les performances obtenues.

La matrice pourrait également être utilisée pour des analyses avec R ou python.



