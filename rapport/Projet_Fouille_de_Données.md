# Projet de Fouille de Données

# Classification de gènes codant pour des partenaires d'un système ABC

*Rivero Alexia et Antoine Guillemin, M1 Bioinformatique et Biologie des Systèmes*

## Introduction

​	Les systèmes de transport à *ATP binding cassette* (ABC) constituent une vaste famille de protéines transmembranaires, dont la présence est établie dans tout le règne vivant depuis les années 1990. En utilisant l’énergie issue de l’hydrolyse de l’ATP, ils permettent l’import ou l’export d’une grande variété de substrats. Ils présentent une architecture conservée, mais leur organisation en domaines peut cependant varier, notamment entre les exporteurs et les importeurs (cf. **Fig. 1**).

![img](https://lh5.googleusercontent.com/wORimms2Tz-wON5TPAOFTMHmIds8VBCT7yjWf4dQ-qSLbcpXf49ri3by8PThE1gUM-FsQQPR-BvzckmhEDVsx7v79K0w1vK-kaSrRw3C5jr1kFpIwghxV7_P_MfJgC9dNPVp28cy)

**Fig. 1. Architecture des transporteurs ABC** Source : http://silico.biotoul.fr/enseignement/m1/datamining/projet/sujet.htmlMSD : *Membrane Spanning Domain*, NBD : *Nucleotide Binding Domain*, SBP : *Solute Binding Protein*



​	Les molécules sont importées ou exportées via un pore à travers la membrane, constitué par les domaines MSD ; l’énergie pour ce transport actif est fourni par les domaines NBD grâce à l’hydrolyse de l’ATP ; enfin, dans le cas des importeurs, le domaine SBD a pour rôle de capturer le substrat et d’amener celui-ci au niveau du pore. Il faut préciser que plusieurs de ces domaines peuvent être codés par un même gène, ou portés par des gènes différents.

​	Le nombre de génomes complets et expertisés ne cesse d’augmenter, ce qui permet donc l’annotation automatique d’autres génomes. Par conséquent, une large quantité d’informations, notamment concernant les systèmes ABC, est disponible sur les bases de données publiques. Ainsi, il pourrait être intéressant d’utiliser ces données pour annoter automatiquement le contenu en systèmes ABC d’un génome. 

​	Dans le cadre de ce projet, il a été choisi d’utiliser des méthodes de fouille de données pour réaliser une classification de gènes selon qu’ils codent ou non pour des partenaires de systèmes ABC. La première étape correspondra à l’analyse des objectifs du projet et des données fournies pour réaliser celui-ci. Ensuite, la conception et les aspects de gestion du projet seront détaillés. Une troisième partie contiendra la réalisation de ces objectifs à proprement parler ; elle montrera également les résultats obtenus selon les choix techniques réalisés, l’interprétation de ces résultats et leur discussion. Enfin, un bilan général sera établi et des perspectives d’amélioration pourront être émises.

## Analyse préliminaire

### Objectifs du projet

​	La fouille de données, ou *data mining*, correspond au processus qui vise à extraire des informations utiles à partir de données volumineuses, notamment en repérant des relations entre ces données ou des motifs (*patterns*).

​	Plusieurs méthodes sont utilisées en fouille de données, dont celle qui est le sujet de ce projet : la classification. Celle-ci consiste à trouver des modèles permettant de distinguer des classes dans les données, dans le but de réaliser une prédiction future de la classe d’un objet. La construction du modèle est réalisée à partir d’un jeu d’apprentissage, dans lequel chaque objet appartient à une classe déjà connue. Ce modèle est appelé classificateur (*classifier*) et fait donc appel à des algorithmes de classification : arbres de décision, classificateur bayésien naïf, analyse discriminante, k plus proches voisins, etc. Pour évaluer les performances du modèle, celui-ci est utilisé sur un jeu de test indépendant du jeu d’apprentissage. En calculant le pourcentage d’objets du jeu de test correctement classés, il est possible de déterminer la précision du classificateur. Enfin, si celle-ci est satisfaisante, le modèle peut être utilisé pour classer de nouvelles données à partir des classes fournies par le jeu d’apprentissage : il s’agit donc d’une méthode d’apprentissage supervisé.

![img](https://lh3.googleusercontent.com/ydG2WeB-76XKP4iqYikk6Y49AJR3ghTs_qhUEZpZ2xGmrWDciacc2EnrMzLK_zxBGp2oPuPbByeVoahmcgKUEwVtJUjKlyKHCrwJurPsSnBBKt0EcWd8nC_1NiUBpK4Ah27mVDfN)

**Fig. 2. Schéma du processus de classification en** **data mining **Source : Dominique Gay. Calcul de motifs sous contraintes pour la classification supervisée. Interface homme-machine [cs.HC]. Université de Nouvelle Calédonie; INSA de Lyon, 2009. Français. tel-00516706



​	Pour ce projet, la classification sera donc réalisée sur des gènes afin de déterminer s'ils codent pour des partenaires ABC (classe : ABC) ou non. Pour mettre en place cette classification, plusieurs étapes ont été définies, correspondant chacune à un objectif particulier.

* **Analyses préliminaires et construction de la matrice avec le logiciel R et/ou un serveur mariadb :**

  ​	Cette étape permettra de visualiser une partie des données, leur organisation dans les différentes tables fournies pour le projet ainsi que les liens entre ces tables. Elle sera également l’occasion de chercher à résumer l’information contenue dans les données pour observer le nombre d’instances de chaque classe, les modalités des différents facteurs, dans le but d’avoir une idée de la pertinence d’un ou plusieurs attributs en ce qui concerne l’objectif principal de classification en gènes codant pour des partenaires ABC. Enfin, si nécessaire, il faudra réduire le volume des données en “nettoyant” celles-ci : cela implique de sélectionner les valeurs significatives, gérer les valeurs manquantes, supprimer les attributs non pertinents, etc. **Ici, l’objectif est d’obtenir un tableau contenant uniquement les données nécessaires à la classification.**

* **Analyse avec KNIME :**

  ​	Pour construire le modèle, l’environnement pour la fouille de données KNIME sera utilisé. Le logiciel permettra de construire plusieurs modèles de classification (arbres de décision, classificateur bayésien naïf) et de les configurer différemment selon les paramètres disponibles. La plateforme KNIME sera également utilisée pour évaluer les performances des modèles créés et déterminer la méthode et les paramètres qui fonctionnent le mieux avec les données. L’évaluation pourra notamment se faire avec un partitionnement et/ou des validations croisées :le partitionnement consiste à utiliser des jeux indépendants, par exemple en divisant l’ensemble du jeu de données en un jeu d’apprentissage (⅔ des données) et un jeu de test (⅓) ;la validation croisée consiste à diviser les données en k partitions, et k-1 partitions seront utilisées par l’apprentissage, la partition restante sera utilisée pour le test. La précision correspondra au rapport du nombre d’objets bien classés lors des k itérations sur le nombre d’objets.

### Exploration des données

​	Les données fournies pour le projet sont issues de plusieurs génomes expertisés constituant la banque de données publique ABCdb (https://www-abcdb.biotoul.fr/). Elles sont réparties en 9 tables, qui correspondent à 9 fichiers .tsv, et concernent :

* les génomes complets (*Strain* et *Chromosome*) ainsi que leur classification taxonomique (*Taxonomy*) ;

* les gènes de chaque génome (*Gene*) ;

* les relations d’orthologie (*Orthology*) ;

* les gènes identifiés comme codant pour des partenaires ABC (*Protein*) ;

* les domaines identifiés (*Conserved Domains*) et les domaines fonctionnels correspondant (*Functional Domains*) ;

* les systèmes réassemblés (*Assembly*).	

Pour identifier si un gène code pour un partenaire ABC ou non, les informations concernant les domaines et les relations d’orthologie seront utiles. **Par conséquent, dans le cadre de ce projet, il a été choisi d’utiliser uniquement les tables** _**Gene**_ ,_**Protein**_**,_** **Conserved Domains,**_ _**Functional Domains**_ _**et** **Orthology.**_ La figure ci-dessous schématise les attributs de chacune de ces tables et les liens entre celles-ci.

![img](https://lh3.googleusercontent.com/ZH4W9kpD9plihZe9fhVGjElyyYseuG4Z5FuwBHazW6Jl-rNm0iICcq21f3zg-c7WZF2ZOeop87nCmWtCgC7qPC8g94e5mITJ1QPwa0TjMezYr17zRh8tgAjAdZqBHG2WUnKSVPU9)

**Fig. 3. Schéma des tables utilisées pour le projet** Source : http://silico.biotoul.fr/enseignement/m1/datamining/projet/sujet.html	

​	A partir de ce schéma, il est déjà possible d’avoir une idée des données qui seront utiles ou non pour le projet :

- les attributs qui correspondent à des longueurs ou des positions de séquences pourraient être éliminés de l’analyse ;
- les attributs qui correspondent à des *e-values* ou des scores de différents types (score, identité, *coverage*, etc.) pourraient être utilisés pour nettoyer les données : par exemple, en sélectionnant uniquement des *e-values* considérées comme significatives.

## Conception 

### Construction de la matrice

​	La construction de la matrice est décrite de manière détaillée dans la documentation utilisateur disponible à l’adresse suivante : https://gitlab.com/rivero.alexia/projet_fouille_abc_guillemin_rivero/-/blob/master/data.preparation/README.md

​	La matrice finale qui sera utilisée pour l’analyse avec le logiciel KNIME contient donc les identifiants de gènes en ligne (individus), et les identifiants de domaines fonctionnels, le type (ABC ou non) et le nombre d’orthologues ABC en colonnes (variables). Cette future analyse cherchera à construire des modèles permettant de prédire la classe de chaque gène à partir de la présence ou l’absence de certains domaines fonctionnels sur ce gène, et/ou du nombre d’orthologues ABC qu’il possède.

### Analyse avec KNIME

​	Le logiciel KNIME va permettre de charger les données, et de construire et d’évaluer plusieurs modèles à partir de celles-ci.

* **Chargement des données**	

​	Premièrement, un nouveau workflow est créé. Le chargement des données est réalisé en ajoutant un noeud *File Reader* qui doit être configuré comme suit :

- *read row IDs* ;

- *read column headers* ;

* *Column delimiter :* </tab> .

Après avoir indiqué le chemin ou l’URL du fichier contenant les données (ici la matrice créée précédemment), le *File Reader* charge les données et en montre un aperçu dans sa fenêtre. Si la matrice a été chargée correctement, le *File Reader* peut être validé.

* **Construction des modèles**	

​	Ensuite, un noeud correspondant au modèle que l’on souhaite construire doit être ajouté. Ici, deux types de *classifiers* sont testés :

* un arbre de décision (*Decision Tree Learner*) ;

* un classificateur bayésien naïf : (*Naive Bayes Learner*).

Le nouveau noeud doit également être configuré. La classe doit correspondre à la colonne *Type* de la matrice, et il est possible de modifier certains paramètres. Par exemple, pour le *Decision Tree Learner* :

* mesure de la qualité : *gain ratio* ou *gini index* ;
* méthode d’élagage : *no pruning* ou MDL. 

Les différents choix possibles seront testés et comparés afin de déterminer lequel est le plus pertinent avec le jeu de données de départ. Enfin, par la suite, un noeud *Predictor* correspondant au modèle choisi sera ajouté. Il prendra en entrée le modèle construit et les données à classer. Aucun paramètre ne sera modifié au niveau de ce noeud.

* **Evaluation des performances**

​	Ici, cette étape sera réalisée à l’aide de validations croisées. Pour cela, il faut premièrement ajouter un noeud *X-Partitioner* entre les noeuds *File Reader* et le noeud *Learner*. Le *X-Partitioner* va diviser le jeu de données en jeux d’apprentissage pour le modèle (*Learner*) et de test pour le *Predictor*. Il est possible de changer le nombre de validations et la méthode d’échantillonnage. Ici seront testées une *3-fold* et une *10-fold cross validations* avec un échantillonnage aléatoire (*random sampling*), un échantillonnage linéaire (*linear sampling*) ou un échantillonnage stratifié (*stratified sampling*). Il faut bien vérifier que la colonne de classification indiquée dans ce noeud corresponde à la colonne *Type*.

​	Le deuxième noeud à ajouter est le *X-Aggregator*, connecté au *Predictor*, qui confronte la classe prédite à la classe connue pour chaque objet testé. Si les paramètres *Target column* et *Prediction column* sont bons, rien ne sera modifié au niveau de ce noeud.

​	Enfin, le noeud *Statistics* ajouté en dernier permettra de visualiser les taux d’erreurs du modèle construit. 
​	

​	Une fois chaque noeud configuré, il faut l’exécuter. Le workflow final doit donc ressembler à celui présenté pour l’arbre de décision de la figure ci-dessous :

![img](https://lh6.googleusercontent.com/_dOCo4_ZNpIUZ-XuG4-rNJxBVXjCqVb_b2rnGF9LK0R2PuIU758P89fyESwpYihtE_oZj3n89lpXxHvstflHsW7ukEM_e20M7FmPjAwLq0fpUwIhFk6F7WEtvd8saSAeKvzXPRuy)

**Fig. 4. Exemple de workflow obtenu avec le logiciel KNIME** Source : KNIME

## 

## Résultats et Discussion

![img](https://lh6.googleusercontent.com/q1Ec7iQymj5907i7WwWHvo17vde0PQcdxgY_3fZbPLhROy1KbfZbLzcRJiZvqA7MKz9w8ykQ6Nsww0hY8e6-15kPdduuz9WqscAFdOEGv1QJaxvhEuQt7GSyLRwwXA61NLMx3s-x)

​	Au vu des différents résultats, même si pour les deux méthodes le taux d’erreur est toujours inférieur à 5%, l’arbre de décision semble être un meilleur parti dans le cas de notre analyse. D’autant plus en prenant un nombre de validations croisées de 10 et les paramètres *Gini index* et *no pruning* (taux d’erreur le plus bas : 0.424%).

​	Lorsqu’on se concentre sur les résultats correspondant à l’arbre de décision, dans tous les cas, quelque soit le type de partitionnement ou le nombre de validations, on obtient un meilleur pourcentage d’erreur lorsqu’on ne réalise pas d’élagage (*no pruning*).

​	Dans le cas du Bayésien naïf, les taux d’erreur sont plus faibles lorsque l’on réalise uniquement un nombre de validation de 3.

​	Pour les deux types de classification, la validation croisée *leave-one-out* n’a pas été réalisée du fait de la taille importante de la matrice individus x variables et donc du temps de calcul important qui aurait été nécessaire.

​	Les résultats obtenus suggèrent que l’arbre de décision est plus adapté à ces données que le classificateur bayésien. Une explication possible pourrait être que le principe du classificateur bayésien naïf repose sur l’hypothèse d’indépendance des attributs, qui mène à des classificateurs optimaux lorsqu’elle est vérifiée. Cependant, on peut supposer que les attributs sont plus ou moins corrélés dans le jeu de données utilisé ici : par exemple, la présence d’un domaine pourrait être corrélée à la présence d’un autre sur la protéine ; la présence de certains domaines fonctionnels pourrait également être corrélée au nombre d’orthologues de type ABC du gène en question. Pour faire face à ce problème d’indépendance, des réseaux bayésiens peuvent notamment être utilisés.

​	Il reste assez difficile de conclure pour la méthode d’échantillonnage la plus adaptée mais le *random sampling* semble produire de bons résultats pour l’arbre de décision notamment.

​	Pour les validations croisées, une *10-fold cross validation* semble produire des taux d’erreurs plus bas qu’une *3-fold* pour l’arbre de décision. Cela pourrait s’expliquer par le fait que dans le premier cas, le jeu de données est divisé en un plus grand nombre de partitions : l’ensemble d’apprentissage est donc plus important et l’opération est répétée 10 fois au lieu de 3, ce qui conduit à une plus grande précision. Cependant, cette explication n’est pas en accord avec les taux d’erreurs produits par le Bayésien naïf qui semble être plus performant avec une *3-fod cross validation*.	

​	Concernant l’arbre de décision, l’élagage semble donc influencer négativement la performance du modèle. Or, l’utilité de l’élagage, et ici plus particulièrement du post-élagage (MDL), est justement de permettre une plus grande précision en évitant que l’arbre ne reflète trop le jeu d’apprentissage (*overfitting*). Pour cela, certaines branches de l’arbre construit sont supprimées car elles peuvent représenter des anomalies. On peut supposer ici que l’élagage supprimerait des branches utiles à la classification et diminuerait donc la précision du modèle. Un élagage pourrait être plus pertinent pour un jeu de données plus important que celui présenté ici.

## Bilan et perspectives 

​	En ce qui concerne les différentes étapes du travail réalisé, l'extraction des données et leur traitement pour la réalisation de la matrice individus x variables a été correctement menée même si de nombreuses difficultés ont pu être rencontrées. En effet, il s'est avéré laborieux de transformer les données concernant les FD_ID afin de noté leur présence ou leur absence. La difficulté principale ayant été de réaliser correctement les jointures entres les différentes tables afin de ne récupérer que les informations pertinentes. De même pour le comptage du nombre d'orthologues ABC des différents gènes étudiés, il a été compliqué d'extraire uniquement les informations correspondant à nos gènes d'intérêt.

​	Les analyses réalisées sur KNIME ont été beaucoup plus facile, au vu des TP/TD réalisées en cours de Fouille de Données, la mise en place du workflow n'a pas été un problème. On peut tout de même noter que la réalisation de tous les tests pour prendre en compte chacunes des conditions nous a pris du temps.

​	Malgré les conditions dans lesquels nous nous trouvions, l'ensemble du projet c'est tout de même déroulée correctement, même s'il aurait été bien plus agréable de pouvoir travailler ensemble. 

​	Afin de compléter ce projet, il aurait pu être interessant de tester d'autres méthodes de classification et donc de construire d'autres modèles (par exemple la méthode des plus proches voisins) afin de voir si l'arbre de décision reste le meilleur choix pour la classification de nos données. 

​	Il aurait également pu être interessant de changer les critères ayant permis de trier nos données, par exemple en modifiant la e-value seuil, ou bien en ne selectionnant que les domaines ayant une fréquence d'apparition supérieur à un seuil fixé préalablement. 

​	De même pour l'évaluation des performance il aurait pu être interessant de tester une autre méthode en plus de la validation croisée, comme par exemple la méthode Hold-out en prennant un sous ensemble des données pour le test et le reste pour l'apprentissage (par exemple 1/3 - 2/3) et de répétés la démarche plusieurs fois afin de calculer l'erreur en faisant la moyenne sur plusieurs hold-out. 

## Gestion du projet

​	Au vu du contexte actuel lié au COVID-19, l’organisation du groupe pour la réalisation du projet en a été fortement impactée. Nous avons donc travaillé chacun de notre côté sur les différentes parties du projet, à notre rythme en fonction de nos emplois du temps respectifs. Nous avons pu organiser des créneaux de travail en commun afin de réfléchir aux différents soucis rencontrés notamment lors de la réalisation de la matrice individus x variables. Nous avons également mis régulièrement en commun nos avancées respectives sur le projet.

​	Nous avons chacun à notre niveau travaillé sur la conception de la matrice : Alexia s’est occupé de la réalisation du data frame et le remplissage des colonnes correspondant au FD_ID, de son côté Antoine a pu récupérer les informations correspondant aux relations d'orthologie (détermination du nombre d’orthologues ABC) et donc réalisé le remplissage de cette colonne afin d’obtenir la matrice finale. La documentation utilisateur pour la conception de la matrice a été réalisée par Antoine pendant qu’en parallèle, Alexia a travaillé sur la finalisation du script permettant d’obtenir la matrice individus x variables. Cette étape de conception de la matrice a été celle nécessitant le plus de temps de travail et d’organisation.

​	Les analyses réalisées sur KNIME ont également été partagées : Antoine a réalisé la première partie consistant en la création du workflow et les premières analyses comparant la classification par arbre de décision et la classification bayésienne dans le cas d’un échantillonnage aléatoire. Alexia a ensuite réalisé les traitements dans le cas des échantillonnages linéaire et stratifié. L’interface de KNIME étant relativement simple à prendre en main, d’autant plus qu’elle avait déjà été abordée en TP, cette étape a pu être réalisée vers la fin du projet, parallèlement à la rédaction du rapport.

​	Concernant la rédaction du rapport, Antoine a plutôt rédigé les premières parties concernant l’introduction et les analyses préliminaires avant la conception de la matrice, afin que le binôme se consacre pleinement à cette étape par la suite. Alexia s’est concentrée sur la rédaction des résultats, de la discussion et a détaillé l’aspect de gestion du projet. La mise en place d’un document partagé s’est avérée utile pour permettre une relecture de la part de chaque membre du binôme.